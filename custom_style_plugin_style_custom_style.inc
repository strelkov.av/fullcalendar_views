<?php

class custom_style_plugin_style_custom_style extends views_plugin_style {
  function option_definition() {
    $options = parent::option_definition();
    $options['example_option'] = array('default' => '');
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['example_option'] = array(
      '#title' => t('Example option'),
      '#type' => 'textfield',
      '#default_value' => $this->options['example_option'],
    );
  }
  
}