<?php

function fullcalendar_views_views_data() {

  $data['fullcalendar_views_sample_data']['table']['group'] = t('Fullcalendar Views sample data');
  $data['fullcalendar_views_sample_data']['table']['base'] = array(
    'field' => 'event_id',
    'title' => t('Custom style'),
    'help' => t('Custom style'),
    'weight' => -10,
  );
  
  $data['fullcalendar_views_sample_data']['event_title'] = array(
    'title' => t('Event title'),
    'help' => t('Event title'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );  
  
  $data['fullcalendar_views_sample_data']['event_date'] = array(
    'title' => t('Event date'),
    'help' => t('Event date'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );    
  
  return $data;
}

function fullcalendar_views_views_plugins() {
  return array(
    'module' => 'custom_style',
    'style' => array(
      'custom_style' => array(
        'title' => t('Custom style'),
        'handler' => 'custom_style_plugin_style_custom_style',
        'path' => drupal_get_path('module', 'custom_style'),
        'theme' => 'custom_style',
        'js' => array(),
        'type' => 'normal',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'even empty' => FALSE,
      ),
    ),
  );
}