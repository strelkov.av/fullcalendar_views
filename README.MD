```sql
CREATE TABLE fullcalendar_views_sample_data (
  event_id INT(11),
  event_title VARCHAR(255),
  event_date VARCHAR(255)
);

INSERT INTO fullcalendar_views_sample_data (`event_id`, `event_title`, `event_date`)
VALUES
('1', 'All Day Event', '2019-08-01'),
('2', 'Long Event', '2019-08-07'),
('3', 'Repeating Event', '2019-08-09'),
('4', 'Conference', '2019-08-11');
```